import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicanPageComponent } from './publican-page.component';

describe('PublicanPageComponent', () => {
  let component: PublicanPageComponent;
  let fixture: ComponentFixture<PublicanPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicanPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicanPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
