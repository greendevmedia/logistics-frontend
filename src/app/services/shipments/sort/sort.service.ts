import { Injectable } from '@angular/core';
import { ShipmentsService } from '../shipments.service';
import { BPClient } from 'blocking-proxy';
import { forEach } from '@angular/router/src/utils/collection';
import { UrlService } from '../../url/url.service';
import { RequestOptions, Headers, Response, Http } from "@angular/http";
import { GlobalService } from '../../global/global.service';


@Injectable()
export class SortService {

  public customs: Map<String, boolean> = new Map<String, boolean>();
  public customers: Map<String, boolean> = new Map<String, boolean>();
  public drivers: Map<String, boolean> = new Map<String, boolean>();
  public name;
  public place;
  public collectionSize;
  public page = 1;
  public pageSize;
  public startLoading;
  public endLoading;
  public startUnloading;
  public endUnloading;

  constructor(private shipmentService: ShipmentsService, private urlService: UrlService, private globalService: GlobalService, private http: Http) {
    this.customs.set("none", false);
    this.customs.set("paper", false);
    this.customs.set("oral", false);

    this.customers.set("buk", false);
    this.customers.set("csd de", false);
    this.customers.set("csd pl", false);
    this.customers.set("ludo", false);
    this.customers.set("spedition-timocom", false);
    this.customers.set("mazury-trans", false);
    this.customers.set("marceli", false);

    this.drivers.set("włodzimierz fertyk", false);
    this.drivers.set("mirosław tecław", false);
    this.drivers.set("leszek tecław", false);
    this.drivers.set("krzysiek chojnacki", false);
    this.drivers.set("adam ciechański", false);
    this.drivers.set("paweł plewiński", false);
    this.drivers.set("adam wilczyński", false);
    this.drivers.set("tomasz czekała", false);

    this.name = null;
    this.place = null;

    this.startLoading;
    this.endLoading;
    this.startUnloading;
    this.endUnloading;

  }

  public chooseType(type) {
    this.customs.set(type, !this.customs.get(type));
    this.sortShipments(30, 0);
  }

  public chooseCustomer(customer) {
    this.customers.set(customer, !this.customers.get(customer));
    console.log(this.customers);
    this.sortShipments(30, 0);
  }

  public chooseDriver(driver) {
    this.drivers.set(driver, !this.drivers.get(driver));
    console.log(this.drivers)
    this.sortShipments(30, 0);
  }

  public chooseName() {
    this.sortShipments(30, 0);
  }

  public choosePlace() {
    this.sortShipments(30, 0);
  }

  public chooseDate() {
    this.sortShipments(30, 0);
  }

  public sortShipments(size, page) {
    if (page == 0) { page = 0; this.page = 1; } else { page = page - 1 }
    let headers = new Headers({ "Authorization": "Bearer " + this.globalService.userAccessDataFromKeycloak.access_token });
    let options = new RequestOptions({ headers: headers });
    let customsList = "";
    let customersList = "";
    let driversList = "";
    let name = null;
    let place = null;
    let startLoading;
    let endLoading;
    let startUnloading;
    let endUnloading;
    this.customs.forEach((value: boolean, key: string) => {
      if (value == true) {
        customsList = customsList + key + ",";
      }
    });
    this.customers.forEach((value: boolean, key: string) => {
      if (value == true) {
        customersList = customersList + key + ",";
      }
    });
    this.drivers.forEach((value: boolean, key: string) => {
      if (value == true) {
        driversList = driversList + key + ",";
      }
    });
    if (this.name === null || this.name === undefined || this.name === "") {
      name = "";
    } else {
      name = "&name=" + this.name;
    }
    if (this.place === null || this.place === undefined || this.place === "") {
      place = "";
    } else {
      place = "&place=" + this.place;
    }
    if (this.startLoading === null || this.startLoading === undefined || this.startLoading === "") {
      startLoading = "";
    } else {
      startLoading = "&startLoading=" + this.startLoading;
    }
    if (this.endLoading === null || this.endLoading === undefined || this.endLoading === "") {
      endLoading = "";
    } else {
      endLoading = "&endLoading=" + this.endLoading;
    }
    if (this.startUnloading === null || this.startUnloading === undefined || this.startUnloading === "") {
      startUnloading = "";
    } else {
      startUnloading = "&startUnloading=" + this.startUnloading;
    }
    if (this.endUnloading === null || this.endUnloading === undefined || this.endUnloading === "") {
      endUnloading = "";
    } else {
      endUnloading = "&endUnloading=" + this.endUnloading;
    }




    let urlShipmentsForManager = this.urlService.apiUrl + "/driver/api/v1/shipments/parameters?dutyType=" + customsList + "&customers=" + customersList + "&drivers=" + driversList + name + place + startLoading + endLoading + startUnloading + endUnloading + "&page=" + page + "&size=" + size + "&sort=unloadingDate,desc";
    this.http.get(urlShipmentsForManager, options).subscribe(
      (res) => {
        this.shipmentService.shipmentsList = [], this.shipmentService.shipmentsList = res.json().content; this.collectionSize = res.json().totalElements; this.pageSize = res.json().size;
      },
      (err) => {
        console.log(err)
      }
    )
  }

}
