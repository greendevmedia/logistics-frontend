import { TestBed, inject } from '@angular/core/testing';

import { ShipmentCustomerService } from './shipment-customer.service';

describe('ShipmentCustomerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShipmentCustomerService]
    });
  });

  it('should be created', inject([ShipmentCustomerService], (service: ShipmentCustomerService) => {
    expect(service).toBeTruthy();
  }));
});
