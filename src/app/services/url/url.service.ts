import { Injectable } from '@angular/core';

@Injectable()
export class UrlService {

  // keycloakUrl: string = "https://42.ip-145-239-85.eu/auth"
  // apiUrl: string = "https://testapilogistics.greendev.in/logistics-0.0.1-SNAPSHOT";
  // apiUrl: string = "http://localhost:8080";
   apiUrl: string = "https://api.plewinski-logistics.pl/logistics-0.0.1-SNAPSHOT";
  // apiUrl: string = "https://api.plewinski-logistics.com/logistics-0.0.1-SNAPSHOT";
  keycloakUrl: string = "https://www.keycloak.plewinski-logistics.com:5513/auth"

  constructor() { }

}
