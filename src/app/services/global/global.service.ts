import { Injectable } from '@angular/core';
import { Response } from '@angular/http/src/static_response';

@Injectable()
export class GlobalService {

  public userName;
  public password;
  public userAccessDataFromKeycloak;
  public isUserLoggedIn: boolean = false;
  public grantType: string = "password";
  public clientID: string= "Logistics";
  public clientSecret: string = "92a5e14c-0588-43ca-96ef-921003eb2bdd";
  public role;

  constructor() { }

}
