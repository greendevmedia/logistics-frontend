import { Injectable } from '@angular/core';
import { GlobalService } from '../global/global.service';
import { RequestOptions, Headers, Response, Http } from "@angular/http";
import { UrlService } from '../url/url.service';

@Injectable()
export class GetDriversService {

  public headers = new Headers({ "Authorization": "Bearer " + this.globalService.userAccessDataFromKeycloak.access_token });
  public options = new RequestOptions({ headers: this.headers });

  public driversList = [];
  public getDriversUrl = this.urlService.keycloakUrl + "/admin/realms/Logistics/groups/5cb292f8-3327-4fd9-9d33-d3bee584e767/members"
  public customersList = [];
  public getCustomerUrl = this.urlService.keycloakUrl + "/admin/realms/Logistics/groups/9435cb00-b464-4c42-bd71-2336eaf18263/members"

  constructor(private urlService: UrlService, private globalService: GlobalService, private http: Http) { }

  getDriversList() {
    // this.http.get(this.getDriversUrl, this.options).subscribe(
    //   res => {
    //     this.JSONtoList(res.json(), this.driversList);
    //   },
    //   err => {
    //     console.log(err)
    //   }
    // )
    this.driversList = ["Włodzimierz Fertyk", "Mirosław Tecław", "Leszek Tecław", "Krzysiek Chojnacki", "Adam Ciechański", "Paweł Plewiński", "Adam Wilczyński", "Tomasz Czekała"]
  }

  getCustomersList() {
    // this.http.get(this.getCustomerUrl, this.options).subscribe(
    //   res => {
    //     this.JSONtoList(res.json(), this.customersList);
    //   },
    //   err => {
    //     console.log(err)
    //   }
    // )
    this.customersList = ["marceli", "csd de", "buk", "csd pl", "ludo", "spedition-timocom", "mazury-trans"]
  }

  JSONtoList(JSONlist, list){
    if(list.length<1)
    for (let item of JSONlist){
      list.push(item.username);
    }
  }
  
}
