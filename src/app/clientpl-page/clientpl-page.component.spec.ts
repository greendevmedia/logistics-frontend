import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientplPageComponent } from './clientpl-page.component';

describe('ClientplPageComponent', () => {
  let component: ClientplPageComponent;
  let fixture: ComponentFixture<ClientplPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientplPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientplPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
